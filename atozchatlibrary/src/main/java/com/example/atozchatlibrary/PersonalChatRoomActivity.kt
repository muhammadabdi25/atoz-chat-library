package com.example.atozchatlibrary

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.atozchatlibrary.R.layout.activity_chat_room_personal

class PersonalChatRoomActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_chat_room_personal)
    }
}